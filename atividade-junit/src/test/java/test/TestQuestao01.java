package test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import br.ucsal.ads20211.testequalidade.atividade02.Questao01;

public class TestQuestao01 {

	@Test
	public void testExibirPrimoNaoPrimo() {

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));

		Boolean primo = false;
		Integer num = 9;
		String resultadoEsperado =   num + " n�o � primo";
		Questao01.exibirPrimoNaoPrimo(num, primo);
		String resultadoAtual = out.toString();
		Assertions.assertEquals(resultadoEsperado, resultadoAtual);

	}


	@Test
	public void testExibirPrimo() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));

		Boolean primo = true;
		Integer num = 23;
		String resultadoEsperado = num + " � primo";
		Questao01.exibirPrimoNaoPrimo(num, primo);
		String resultadoAtual = out.toString();
		
		Assertions.assertEquals(resultadoEsperado, resultadoAtual);


	}
	
	@Test
	public void testObterNumeroFaixa() { 

		ByteArrayInputStream in = new ByteArrayInputStream("0\n1001\n-0.0\n12".getBytes());
		System.setIn(in);
		Integer resultadoEsperado = 12;
		Integer resultadoAtual = Questao01.obterNumeroFaixa();

		Assertions.assertEquals(resultadoEsperado, resultadoAtual);

	}


	@Test
	public void testObterNumeroFaixa2() { 

		ByteArrayInputStream in = new ByteArrayInputStream("0\n1001\n-0.0\n300".getBytes());
		System.setIn(in);
		Integer resultadoEsperado = 300;
		Integer resultadoAtual = Questao01.obterNumeroFaixa();

		Assertions.assertEquals(resultadoEsperado, resultadoAtual);

	}


	@ParameterizedTest(name = "{index} verificarPrimo ({0}) = {1}")
	@CsvSource({"13, true", "40, false", "15,false", "17, true", "3, true",})
	public void testarNumeros(Integer num, Boolean resultadoEsperado) { 
		System.out.println("teste de:  " + num + " = " + resultadoEsperado);

		Boolean resultadoAtual = Questao01.verificarPrimo(num);
		Assertions.assertEquals(resultadoEsperado, resultadoAtual);

	}

}
